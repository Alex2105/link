package com.example.alscon.link;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Set;


public class CustomActivity extends Activity {
    String fileName;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_view);
        Uri url = getIntent().getData();
        Intent intent = getIntent();
        Set<String> keys = intent.getExtras().keySet();
        for (String key : keys) {
            fileName = intent.getStringExtra(key);

        }

        String urlAdress = url.toString();
        String dirPath = url.toString();


        saveFile(fileName);
        saveFile(urlAdress);
        newLine();


    }


    private void saveFile(String data) {
        try {
            File myFile = new File("/sdcard/mysdfile.xml");
            if (!myFile.exists()) {
                myFile.createNewFile();
            }
            FileOutputStream fOut = new FileOutputStream(myFile, true);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);
            myOutWriter.append("&nbsp");
            myOutWriter.close();
            fOut.close();
            Toast.makeText(getBaseContext(),
                    "Done writing SD 'mysdfile.xml'",
                    Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(getBaseContext(), e.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }


    }

    private void newLine() {
        try {
            File myFile = new File("/sdcard/mysdfile.xml");
            if (!myFile.exists()) {
                myFile.createNewFile();
            }
            FileOutputStream fOut = new FileOutputStream(myFile, true);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append("<br/>");
            myOutWriter.close();
            fOut.close();
        } catch (Exception e) {
            Toast.makeText(getBaseContext(), e.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    }


}
